const languages = [
    {
        name: 'English',
        locale: 'en',
    },
    {
        name: 'Italiano',
        locale: 'it',
    },
    {
        name: 'Српски',
        locale: 'sr',
    },
    {
        name: 'Español',
        locale: 'es',
    },
];

export default languages;
