Gruppi di pietre possono essere catturati una volta circondati.
Le pietre nere qui sotto sono in una situazione precaria - 
nessun posto dove scappare e in attesa di essere catturate da bianco.

Board::trouble

---

Nero può di nuovo giocare liberamente. Bianco non risponderà, ma sta
attento alle mosse illegali.

Board::heaven

## Due occhi

Considera la situazione qui sotto.

Board::life

Nonostante le pietre nere siano circondate, bianco non può catturarle.
Ci sono ancora due spazi vuoti da riempire, ma entrambi sono mosse illegali. 

Chiamiamo questi spazi interni separati **occhi**. Una volta che un gruppo
fa due **occhi**, non può essere catturato. Possiamo considerare il gruppo
**incodizionatamente vivo**.

Prova di nuovo a catturare le pietre bianche sul goban.

Board::unkillable

Puoi catturarle?

Scommetto di no! Sono incondizionatamente vive perchè hanno 2 occhi.
