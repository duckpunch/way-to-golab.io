Questo è un tutorial di base per il gioco del [Go][go-game] adattato dal 
[Interactive Way to Go][iwtg] di Hiroki Mori

### Come aiutare

* Conosci un'altra lingua?  Sei disponibile ad aiutare a tradurre? Per favore
  contattami aprendo un [issue on GitLab][gitlab]. Non è necessario saper programmare!
* Hai trovato un bug?  Apri un [issue][gitlab]!

### Riconoscimenti

* Hiroki Mori ha scritto sia il contenuto che il codice del tutorial originale
  [Interactive Way to Go][iwtg] nel 1997, facendo conoscere il gioco a migliaia di nuovi giocatori.
* Con il permesso di Hiroki, [duckpunch][] ha riscritto il codice usando metodi moderni nel 2019,
  mantenendo la maggior parte del stesso contenuto originale.

[duckpunch]: https://duckpunch.org
[gitlab]: https://gitlab.com/way-to-go/way-to-go.gitlab.io
[go-game]: https://en.wikipedia.org/wiki/Go_(game)
[iwtg]: https://web.archive.org/web/20210831172631/http://playgo.to/iwtg/en/
