Negra está bloqueado en el esquina izquierda inferior. El grupo será rodeado si 
no haces nada. Pero, el grupo será seguro si puede capturar una de las piedras 
blancas circundantes. 

Board::kamitori

Si negra es exitosa, tendrá que ser seguro por tener huecos bastantes, u *ojos* 
donde blanca no puede jugar. 

---

El grupo de blanca de la esquina derecha inferior todavía no tiene dos ojos. 
Pero, si las piedras marcadas son capturadas, blanca vivirá fácilmente. 

Asegúrate de que no lo suceda!

Board::oiotoshi

Si negra juega la jugada incorrecta, no se puede hacer nada para salvar las piedras. 

Incluso si conectas, aun así terminas perdiendo las piedras. Este posición
desafortunada se llama *oitoshi*.  

Tienes que escaparse *antes* del atari!
