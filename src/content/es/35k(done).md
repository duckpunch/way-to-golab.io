## Acercarse el fín del juego

A medida que el juego se acerca el fín y ambos los territorios de blanca y negra
son fijados, la sola cosa tienes que hacer es decidir las fronteras claramente. 
  
At this stage, you must try to push the border toward the opponent's side,
widening your territories as large as possible while reducing the opponent's.

---

Negra ocupa el lado izquierdo y blanca controla la derecha. Pero, todavía hay
algunos lugares en que la frontera no está clara. 

Dónde está el punto que puedes ganar lo más beneficio?

Board::yose-1

---

Negra tiene el izquierdo y blanco tiene la derecha. El juego es casi terminado, pero
ten cuidado hasta el final!

Blanca acaba de jugar la jugada marcada. Si la ignoras, su territorio precioso 
será arruinado.

Board::yose-2
