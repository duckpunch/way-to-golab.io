## Capture the atari stones!

Normally, a 19x19 board is used, but the smaller 9x9 board is generally
recommended for beginners.

As black, try capturing white's stones.

Board::capture1

---

Two white stones are in atari. Capture them with one move!

Board::capture2

---

Even if there are other stones nearby, you can apply the same principle to capture.

Find the move to capture white!

Board::capture3

---

Repeat above problems until you understand completely.

If you do, I hereby grant you **50 Kyu** - the lowest rating of my personal rating system :)

## About the ranking system of Go

In the amateur Go world, 30 kyu is usually the lowest rank. The smaller the kyu,
the stronger the player.

1 dan comes after 1 kyu. The higher the dan, the stronger the player.

Dan players are considered to be quite strong.
