### Kakari and Shimari - corners are profitable

You will learn how to play in the beginning - especially, the negotiation
around the corners.

You can see how real games are started on the 13x13 board on the left. Click
Next button repeatedly. You can step backwards with Back button.  First, each
side played a position near two corners.

The first move by black is called 3-3 point ( or san-san) because it is located
on the third line of vertical and horizontal lines counting from the closest
corner. This move guarantees a position that can obtain the corner.

On the next play, white also played on 3-3 point.

The next move by black is called the 4-4 point ( or hoshi) where it is marked
with a small black circle. This point can surround a larger corner than a play
on the 3-3 point. However, it can be invaded more easily because it has more
spaces between it and the edge of the board.

The fourth move by white is also a popular one.

Of course, you can play anywhere on the board if it is not illegal.  Usually, a
game proceeds from the corners to sides and eventually to the middle.

Though corners are the best places to get territory, no one would make a move
at as M2 or N1 because that would be too small.

Board::corners0

---

Continuing from the above...

On the fifth move, black reinforced his lower left corner by playing on F3.
These kind of strengthening moves are called Shimari( or 'enclosure' moves)

On the next move, white also strengthened her lower right corner with a
different kind of shimari.

Next, black put a stone near a white stone.(J10) This move implies that if
white ignores it, black will invade the corner by playing somewhere like L11.
This kind of approach move is called Kakari(attacking, or aiming).

Then white "touched" the black stone by playing J11 to strike back. After that,
each side strengthened themselves.(K11,G10)

White thought that it was looking as if the left side of the board would be
black's territory. Thus she invaded in the left side by herself to prevent
that! These moves are called Uchikomi (smashing, striking, or invading) - By
the way, you don't have to remember these Japanese words right now.

This is one of example showing how games are started.

Keep in mind that Go is a sharing game - you can't get everything.  You have to
yield, you have to sacrifice, you have to apologize if you were too greedy.  If
you were too aggressive, you will lose everything, literally!

Board::corners1
